let playing = false;
let volume = 0;

const audio = document.querySelector('#audio');


const draggable = Draggable.create('#knob', {
  type: "rotation",
  inertia: true,
  bounds:{maxRotation:360, minRotation:0},
  onDrag: function() {
      volume = this.rotation / 360;
      
      console.log(volume);
      document.querySelector('#audio').volume = volume;
  }
});

document.querySelector('body').addEventListener('click', function() {
    if (!playing) {
        audio.volume = volume;
        audio.play();
        control.innerText = 'Turn me';
        playing = true;
    }    
});