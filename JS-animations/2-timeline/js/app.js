gsap.timeline()
    .from('.products', { opacity: 0, duration: 1 })
    .from('.products h2', { opacity: 0, scale: 0, ease: 'back', duration: 0.5 })
    .from('.products .product-card', { y: 160, stagger: 0.1, opacity: 0, ease: 'back', duration: 0.8 })
    
