/*eslint-env es6*/
/*eslint-env browser*/

const intro = document.querySelector('.intro');
const video = intro.querySelector('video');
const title = intro.querySelector('h1');
const text = intro.querySelector('p');
const images = intro.querySelectorAll('img')

const controller = new ScrollMagic.Controller();

let previousSequence;

// Zoom out from normal to 80% size
const zoomAnim = gsap.to(images, { scaleX: 0.80, scaleY: 0.80 });

// Create scene to zoom out the images
let scene = new ScrollMagic.Scene({
    duration: 3000,
    triggerElemnt: intro,
    triggerHook: 0
})
.addIndicators() // only for development, show trigger and end of scene
.setPin(intro)
.setTween(zoomAnim)
.addTo(controller);

// Change the image based on scene position, 1 scroll is 100px so e.scrollPos/10 changes 10 times per scroll 
scene.on('update', e => {
    let currentSequence = Math.round(e.scrollPos/10) + 1;
    
    // No need to alter style if the sequence didn't change and if the sequence is at the end
    if (previousSequence !== currentSequence && currentSequence <= 132) {
        previousSequence = currentSequence;
        
        for (const image of images) {
            image.style.display = 'none';
        }
        
        intro.querySelector('#hero-ligthpass-' + toFourDigits(currentSequence)).style.display = 'block';
    }
});

// Animations for different text 
const timeline = gsap.timeline();
timeline.to(title, { y: '-60vh', duration: 5 });
timeline.fromTo(text, { opacity: 0, y: 50 }, { opacity: 1, y: 0, duration: 2 });
timeline.addPause(7);
timeline.to(text, { opacity: 0, duration: 2 });
timeline.addPause(2);

// Create sepatate scene for text animations
let scene2 = new ScrollMagic.Scene({
    duration: 3000,
    triggerElemnt: intro,
    triggerHook: 0
})
.setTween(timeline)
.addTo(controller);

const toFourDigits = number => {
    if (number < 10) {
        return '000' + number;
    } else if (number < 100) {
        return '00' + number;
    } else if (number < 1000) {
        return '0' + number;
    }
    
    return '' + number;
}