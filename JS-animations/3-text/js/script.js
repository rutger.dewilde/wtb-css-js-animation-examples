const words = ["Web Developer", "Software Developer", "Network Engineer", "IOT Developer", 'Telecommunications Engineer'];

let cursor = gsap.to('.cursor', { opacity: 0, ease: 'power2.inOut', repeat: -1 });
let masterTimline = gsap.timeline({ repeat: -1 });

words.forEach(word => {
  let timeline = gsap.timeline({ repeat: 1, yoyo: true, repeatDelay: 1 });
  timeline.to('.text', { duration: 1.5, text: word });
  masterTimline.add(timeline);
});